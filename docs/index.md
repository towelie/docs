# Introduction
Towelie is a Smart Towel RG-400 first manufactured by Tynacorp to make people dry as needed - or, according to Tynacorp's enemies, to make them too dry. After discovering marijuana, he got high and sort of wandered off however, and soon found himself in South Park, where he eventually began trailing the boys and getting them embroiled in a TynaCorp plot involving Gary, Towelie's successor, allowing Towelie to gain compassion before escaping.

Now free to live his own life he is now a dedicated (to his best abilities) to manage discord servers yet still continuing to give towel advice to people in South Park to this day.

## Stack
Towelie Bot is a small Discord bot project written in React(frontend) + Node(backend) being hosted in South Park Genetic Engineering Ranch.

## API Term
1. Free for non-commercial use. 
2. Using the Towelie API as a backup or data storage service is strictly prohibited. 
3. ‘Hoarding’ or mass collection of data from the Towelie API is strictly prohibited. 
4. Applications or services must comply with our naming guidelines.
5. Prohibited from use within competing noncomplementary services of the same nature, including but not limited to Towelie list/tracker services. Competing services may be authorized on request if they provide significant and sustained integration/syncing with the Towelie API.

## Adult content and application stores

Many app stores such as the Apple app store prohibit 18+ content. If you’re planning on providing your client via a third-party service you should check their official stance on the matter.

Where we can, we provide an `isAdult` boolean in the media API data, however, we cannot 100% ensure that this will always be accurate or that our definition of ‘adult’ content meets the same standards of other services. Specifically ‘Ecchi’ shows are not included in the ‘adult’ boolean, which is known to have caused issues with Google Adsense and the Apple app store in the past.

We also cannot ensure the data provided by our users will always be non-adult \(primarily activity and forum data\).

We recommend you do additional checks and limit the data shown on your client to comply with the standards of any services you may be using.

## The Team

|                      |                                                                                                                |
|----------------------|----------------------------------------------------------------------------------------------------------------|
| ![](images/codevski.jpg) | Hi, I'm **Codevski**. I'm making the first version of Towelie bot.                                             |